<?php
	error_reporting(E_ALL);
	empty($_SESSION)? session_start() : print "";
	include("./BD/info_bd.php");
?>

<!doctype html>
<html lang="fr">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<title> HereOuiGo - voyagez tranquille </title>
		<link rel="icon" href="./WebContents/icon.png" sizes="64x64" />
		<link rel="stylesheet" href="styles.css"/>
		<script src="./Scripts/monscript.js"></script>

		<!--[if lt IE 9]>
			<script src="./Scripts/html5shiv.js"></script>
		<![endif] -->
	</head>
	<body>
	<?php
		include("./include/header.php");
	?>
	<div id="main">
	<?php
		if(isset($_POST['submit']) && isset($_SESSION['auth'])){
			$immat =strtoupper($_POST['immatriculation']);
			$marque =strtoupper($_POST['marque']);
			$modele =strtoupper($_POST['modele']);
			$type_carburant = $_POST['type_carburant'];
			$nb_places = intval($_POST['nb_place']);
			try{
				$email=$_SESSION['mail'];
					// Connexion à la BDD
				$bdd = new PDO('mysql:host=localhost;dbname=bdd_hereouigo;charset=utf8', $username, $password);
				// On recherche si le véhicule n'existe pas déjà, sinon on l'ajoute.
				$req_verif=$bdd->prepare("SELECT * FROM vehicule WHERE immatriculation=:immatriculation;");
				if($req_verif->execute(array('immatriculation' => $immat))){
					$data = $req_verif->fetch();
					if(count($data) <= 1){
						$req_verif=$bdd->prepare("INSERT INTO vehicule (immatriculation,marque,modele,type_carburant,nombre_place) VALUES(:immatriculation, :marque, :modele, :type_carburant, :nombre_place);");
						if(!$req_verif->execute(array('immatriculation' => $immat, 'marque'=>$marque, 'modele'=>$modele, 'type_carburant'=>$type_carburant, 'nombre_place'=>$nb_places))){
							include("./include/formulaire_vehicule.php");
							echo "
								<div class='error_box'>
								<p>Une erreur s'est produite lors de l'execution de votre demande, veuillez réessayer !</p>
								</div>";
							exit();
						}
					}
					// On ajoute dans membre_véhicule
					$req_verif=$bdd->prepare("INSERT INTO membre_vehicule (mail,immatriculation) VALUES(:mail,:immatriculation);");
					if($req_verif->execute(array('mail'=>$email, 'immatriculation' => $immat))){
						print "<div class='valid_box'><p>Votre vehicule a bien été enregistré !</p>
						<p>Vous pouvez désormais <a href='add_trajet.php'>proposer un trajet !</a></p></div>";
					}
					else{
						include("./include/formulaire_vehicule.php");
						echo "
							<div class='error_box'>
							<p>Une erreur s'est produite lors de l'execution de votre demande, veuillez réessayer !</p>
							</div>";
					}
				}
				else{
					include("./include/formulaire_vehicule.php");
					echo "
						<div class='error_box'>
						<p>Une erreur s'est produite lors de l'execution de votre demande, veuillez réessayer !</p>
						</div>";

				}
				// On oublie pas de  fermer le curseur d'analyse des résultats après l'utilisation 
				$req_verif->closeCursor();
 
				// Déconnexion de la BDD
				unset( $bdd );
				
			}
			catch(PDOException $e){
				print"Erreur ! : ".$e->getMessage()."</br>";
				die();
			}

		}
		elseif(isset($_SESSION['auth'])){
			if(isset($_GET['msg'])){
				echo "
						<div class='error_box'>
						<p>Veuillez renseigner un vehicule avant de pouvoir proposer un trajet!</p>
						</div>";
			}

			include("./include/formulaire_vehicule.php");
		}
		else{
				echo "
					<div class='error_box'>
					<p>Vous n'avez pas accès à cette demande.</p>
					<a href='index.php'> Retourner à l'accueil </a>
					</div>";
		}

	?>

		
	</div>
		<?php
			include("./include/footer.php");
		?>
	</body>
</html>