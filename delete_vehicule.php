<?php
    error_reporting(E_ALL);
    empty($_SESSION)? session_start() : print "";
    include("./BD/info_bd.php");?>
<!doctype html>
<html lang="fr">
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <title> HereOuiGo - voyagez tranquille </title>
        <link rel="icon" href="./WebContents/icon.png" sizes="64x64" />
        <link rel="stylesheet" href="styles.css"/>

        <!--[if lt IE 9]>
            <script src="./Scripts/html5shiv.js"></script>
        <![endif] -->
    </head>
    <body>


<?php
    if(isset($_POST['immatriculation']) && isset($_SESSION['auth'])){
            $email=$_SESSION['mail'];
            $immat=strtoupper($_POST['immatriculation']);
            try{
                // Connexion à la BDD
                $bdd = new PDO('mysql:host=localhost;dbname=bdd_hereouigo;charset=utf8', $username, $password);
                // Supprime d'abord dans membre_vehicule
                $req_verif =$bdd->prepare("DELETE FROM membre_vehicule
                    WHERE mail = :email AND immatriculation=:immatriculation;");
                if($req_verif->execute(array('email' => $email,'immatriculation'=> $immat))){
                    $req_verif =$bdd->prepare("SELECT * FROM vehicule WHERE immatriculation=:immatriculation;");
                    if($req_verif->execute(array('immatriculation'=>$immat))){
                        $data = $req_verif->fetchAll();
                        var_dump($data);
                        if(count($data) == 2){
                            $req_verif = $bdd->prepare("DELETE FROM vehicule WHERE immatriculation=:immatriculation;");
                            if(!$req_verif->execute(array('immatriculation' => $immat))){
                                /*echo "
                                    <div class='error_box'>
                                    <p>Une erreur s'est produite lors de l'execution de votre demande, veuillez réessayer !</p>
                                    </div>"; */
                                    header("Location: profil.php?pseudo={$_SESSION['pseudo']}");
                            }
                            else{
                                /*echo "
                                    <div class='valid_box'>
                                    <p>Le véhicule ".$immat." a bien été supprimé !</p>
                                    </div>";*/
                                    header("Location: profil.php?pseudo={$_SESSION['pseudo']}");
                            }
                        }else{
                           /* echo "
                                    <div class='valid_box'>
                                    <p>Le véhicule ".$immat." a bien été supprimé de votre compte !</p>
                                    </div>";*/
                                    header("Location: profil.php?pseudo={$_SESSION['pseudo']}");
                        }
                    }
                    else{
                        /*echo "
                            <div class='error_box'>
                            <p>Une erreur s'est produite lors de l'execution de votre demande, veuillez réessayer !</p>
                            </div>";*/
                        header("Location: profil.php?pseudo={$_SESSION['pseudo']}");
                    }
                }
                else{
                   /* echo "
                    <div class='error_box'>
                    <p>Une erreur s'est produite lors de l'execution de votre demande, veuillez réessayer !</p>
                    </div>";*/
                    header("Location: profil.php?pseudo={$_SESSION['pseudo']}");
                }
                // On oublie pas de  fermer le curseur d'analyse des résultats après l'utilisation 
                $req_verif->closeCursor();
 
                // Déconnexion de la BDD
                unset( $bdd );
            }
            catch(PDOException $e){
                print"Erreur ! : ".$e->getMessage()."</br>";
                die();
            }
    }
    else{
        echo "
            <div class='error_box'>
            <p>Vous n'avez pas accès à cette demande.</p>
            <a href='index.php'> Retourner à l'accueil </a>
            </div>";
   }

?>
</body>