<?php
    error_reporting(E_ALL);
    empty($_SESSION)? session_start() : print "";
?>

<!doctype html>
<html lang="fr">
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <title> HereOuiGo - voyagez tranquille </title>
        <link rel="icon" href="./WebContents/icon.png" sizes="64x64" />
        <link rel="stylesheet" href="styles.css"/>
        <script src="./Scripts/monscript.js"></script>

        <!--[if lt IE 9]>
            <script src="./Scripts/html5shiv.js"></script>
        <![endif] -->
    </head>
    <body>
        <?php
            include("./include/header.php");
        ?>
        <div id="main">
        <?php
        if(isset($_GET['content'])){
            switch($_GET['content']){
                case "condutil":
                     echo "
                        <h2>Conditions générales d'utilisation du site HereOuiGo.com</h2>

                        <h3>ARTICLE 1 : Objet</h3>

                        <p>Les présentes « conditions générales d'utilisation » ont pour objet l'encadrement juridique des modalités de mise à disposition des services du site HereOuiGo.com et leur utilisation par « l'Utilisateur ».</p>

                        <p>Les conditions générales d'utilisation doivent être acceptées par tout Utilisateur souhaitant accéder au site. Elles constituent le contrat entre le site et l'Utilisateur. L’accès au site par l’Utilisateur signifie son acceptation des présentes conditions générales d’utilisation.<p>

                        <h4>Éventuellement :</h4>

                            <p>En cas de non-acceptation des conditions générales d'utilisation stipulées dans le présent contrat, l'Utilisateur se doit de renoncer à l'accès des services proposés par le site.<p>

                            <p>HereOuiGo.com se réserve le droit de modifier unilatéralement et à tout moment le contenu des présentes conditions générales d'utilisation.</p> 
                            <p>Etc ...</p>";

                        break;
                case "polconf":
                    echo "<h2>1.Collecte de l’information</h2>

                        <p>Nous recueillons des informations lorsque vous vous inscrivez sur notre site, lorsque vous vous connectez à votre compte, faites un achat, participez à un concours, et / ou lorsque vous vous déconnectez. Les informations recueillies incluent votre nom, votre adresse e-mail, numéro de téléphone, et / ou carte de crédit.</p>

                        <p>En outre, nous recevons et enregistrons automatiquement des informations à partir de votre ordinateur et navigateur, y compris votre adresse IP, vos logiciels et votre matériel, et la page que vous demandez. </p>";
                    break;

                case "cookies":
                    echo "<h2>Informations générales sur l'utilisation des Cookies/Traceurs</h2>

                        <p>Le site que vous visitez utilise des traceurs (cookies). Ainsi, le site est susceptible d'accéder à des informations déjà stockées dans votre équipement terminal de communications électroniques et d'y inscrire des informations.</p>

                        <p>Le site utilise exclusivement des traceurs dits strictement nécessaires, qui ne nécessitent pas votre consentement préalable.</p>

                        <p>Nous utilisons ces traceurs pour _____ [préciser la finalité, par exemple : permettre et faciliter la navigation sur le site notamment en mémorisant vos préférences de navigation définis au cours de votre session / fournir les services que vous demandez expressément tels... [préciser de quels services il s'agit] / réaliser des statistiques anonymes de visites].</p>

                        <p>Ces traceurs ne peuvent pas, techniquement, être désactivés depuis le site. Vous pouvez néanmoins vous opposer à l'utilisation de ces traceurs, exclusivement en paramétrant votre navigateur. Ce paramétrage dépend du navigateur que vous utilisez, mais il est en général simple à réaliser : en principe, vous pouvez soit activer une fonction de navigation privée soit uniquement interdire ou restreindre les traceurs (cookies). Attention, il se peut que des traceurs aient été enregistrés sur votre périphérique avant le paramétrage de votre navigateur : dans ce cas, effacez votre historique de navigation, toujours en utilisant le paramétrage de votre navigateur.</p>

                         

                        <p>L'utilisation des traceurs est régie par l'article 32 II de la loi n° 78-17 du 6 janvier 1978, transposant l'article 5.3 de la directive 2002/58/CE du parlement européen et du conseil du 12 juillet 2002 modifiée par la directive 2009/136/CE.</p>

                        <p>Pour en savoir plus sur les cookies et traceurs, nous vous invitons à consulter le site de la CNIL : www.cnil.fr.</p>";
                    break;
                case "nous":
                    echo "<h2>Notre entreprise</h2>
                            
                        <p> Nous sommes des étudiants de la Faculté des Sciences de l'Université de Montpellier et nous apprenons à faire un site web donc techniquement nous ne sommes pas une entreprise !</p>";
                    break;
                case "presse":
                    echo "<h2>On parle de nous !</h2>
                        <p> C'est vrai que cela pourrait être potentiellement pas trop mal d'avoir de la visibilité dans la presse mais bon on ne peut pas tout avoir non plus !</p>";
                    break;
                case "recrue":
                    echo "<h2>Nos offres d'emplois</h2>
                        <p> Un peu d'aide ne serait vraiment pas de refus : pour remplir les différentes pages plus sérieusement, tester le site en condition réelle, fixer certains bugs potentiels. </p>";
                    break;
                case "faq":
                    echo "<h2>La Foire aux Questions</h2>
                        <p> Note pour plus tard : Penser à la faire, ça peut être utile un jour.</p>";
                    break;

            }

            }
        
            ?>
    	</div>

    <?php 
        include("./include/footer.php");
    ?>
    </body>
</html>
