<?php
error_reporting(E_ALL);
empty($_SESSION)? session_start() : print "";
include("./BD/info_bd.php");
?>

<!doctype html>
<html lang="fr">
<head>
	<meta charset="utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title> HereOuiGo - voyagez tranquille </title>
	<link rel="icon" href="./WebContents/icon.png" sizes="64x64" />
	<link rel="stylesheet" href="styles.css"/>
	<script src="./Scripts/monscript.js"></script>

		<!--[if lt IE 9]>
			<script src="./Scripts/html5shiv.js"></script>
			<![endif] -->
		</head>
		<body>

			<?php
			include("./include/header.php");
			?>
			<div id="main">
				<?php
				if(isset($_POST['submit']) && isset($_SESSION['admin'])){
					$email=$_POST['pseudo'];
					$now = date("Y-m-d");
					try{
					// Connexion à la BDD
						$bdd = new PDO('mysql:host=localhost;dbname=bdd_hereouigo;charset=utf8', $username, $password);
					// Vérifier que le membre existe déjà dans la BDD
						$req_verif =$bdd->prepare("SELECT * 
							FROM membre 
							WHERE mail = :email;");
						if($req_verif->execute(array('email' => $email))){
							$data = $req_verif->fetch();
							if(count($data) > 1){
								$req_verif = $bdd->prepare("INSERT INTO blacklist (mail) VALUES(:email);");
								if($req_verif->execute(array('email' => $email))){
									echo "
									<div class='valid_box'>
									<p>Cet utilisateur est désormais bloqué !</p>
									<p><a href='index.php?admin=TRUE'> Retour à l'administration </a></p>
									</div>";


									/* On supprime ses offres */
									$req_verif = $bdd->prepare("SELECT offre.id_offre FROM offre, trajet WHERE offre.id_membre = :email AND offre.id_trajet = trajet.id_trajet AND trajet.date_trajet >= :now;");
									if($req_verif->execute(array("email" => $email, "now"=> $now))){
										$data = $req_verif->fetchAll();
										if(count($data) > 0){
									//Pour chaque trajet qu'il propose on récupère les mails des passagers 
											foreach($data as $ligne){
												$req_verif = $bdd->prepare("SELECT passager.mail FROM passager WHERE passager.id_offre = :offre;");
												if($req_verif->execute(array('offre' => $ligne['id_offre']))){
													$mails = $req_verif->fetchAll();
													if(count($mails) > 0){
													// Une offre a des passagers, on envoie un mail 
														foreach($mails as $mail){
															$a = $mail['mail'];
															$sujet = "Annulation d'un trajet";
															$message = "Cher utilisateur, \n Un trajet a été annulé. Veuillez consulter la rubrique 'Mes Trajets' accessible depuis votre compte. \n Veuillez nous excuser du désagrément occasionné, \n Bien cordialement, \n L'équipe HereOuiGo.";
															$entete = "De:  HereOuiGo\r\n";
															$entete .= "Content-type: text/plain; charset=UTF-8" . "\r\n";
															if(@mail($a,$sujet,$message,$entete)){
															// Bin ouais c'est cool
																print "";
															}
															else{
																$error_message = true;
																$error_str .= $mail['mail']. " ";
															}
														}
														if($error_message){
															echo "
															<div class='error_box'>
															<p>Une erreur est survenue lors de l'envoi des mails d'annulation aux passagers</p>
															<p>Elle concerne : {$error_str}</p>
															</div>";
														}

													// puis on les supprime de passager
														$req_verif = $bdd->prepare("DELETE FROM passager WHERE passager.id_offre = :id_offre;");
														$req_verif->execute(array("id_offre" => $ligne['id_offre']));


													}
												// L'offre n'a pas de passagers ou bien on a supprimé les passagers, on l'efface
													$req_verif = $bdd->prepare("DELETE FROM offre WHERE offre.id_offre = :id_offre ;");
													$req_verif->execute(array("id_offre" => $ligne['id_offre']));

												}else{
													print "";
												//Erreur execution requete recherche de passager
												}
											}
										}	

									}else{ 
								//Erreur
										print "";
									}
								// Il n'a aucune offre de disponible sur le site

									// On l'efface des offres auxquelles il est passager

									$req_verif = $bdd->prepare("SELECT passager.id_offre FROM passager, offre, trajet WHERE passager.id_offre = offre.id_offre AND offre.id_trajet = trajet.id_trajet AND passager.mail=:email;");
									if($req_verif->execute(array("email" => $email))){
										$offres = $req_verif->fetchAll();
										foreach($offres as $offre){
											$num_offre = $offre['id_offre'];
											$req = $bdd->prepare("UPDATE offre SET nb_places = nb_places + 1 WHERE id_offre = :id_offre;");
											if(!$req->execute(array("id_offre" => $num_offre))){
											//Erreur
												print "";
											}else{
											// On l'efface du trajet
												$req = $bdd->prepare("DELETE FROM passager WHERE passager.mail=:email AND passager.id_offre = :id_offre;");
												if(!$req->execute(array("email" => $email, "id_offre" => $num_offre))){

												}
											}
											$req->closeCursor();
										}

									}else{
										echo "
										<div class='error_box'>
										<p>Une erreur est survenue lors de la recherche des offres du membre à supprimer.</p>
										<p>Veuillez contacter la personne en charge du développement de l'application</p>
										</div>";
									}


								// 
								}else{
									include("./include/formulaire_blacklist_membre.php");
									echo "
									<div class='error_box'>
									<p>Une erreur s'est produite lors de l'execution de votre demande, veuillez réessayer !</p>
									</div>";
								}
							}
							else{
								include("./include/formulaire_blacklist_membre.php");
								echo "
								<div class='error_box'>
								<p>Cet utilisateur n'est pas référencé dans la base de donnée.</p>
								</div>";
							}
						}
						else{
							include("./include/formulaire_blacklist_membre.php");
							echo "
							<div class='error_box'>
							<p>Une erreur s'est produit, veuillez réessayer !</p>
							</div>";
						}
					// On oublie pas de  fermer le curseur d'analyse des résultats après l'utilisation 
						$req_verif->closeCursor();

					// Déconnexion de la BDD
						unset( $bdd );
					}
					catch(PDOException $e){
						print"Erreur ! : ".$e->getMessage()."</br>";
						die();
					}
				}
				elseif(isset($_SESSION['admin'])){
					include("./include/formulaire_blacklist_membre.php");
				}
				else{
					echo "
					<div class='error_box'>
					<p>Vous n'avez pas accès à cette demande.</p>
					<a href='index.php'> Retourner à l'accueil </a>
					</div>";
				}

				?>


			</div>
			<?php
			include("./include/footer.php");
			?>
		</body>
		</html>
