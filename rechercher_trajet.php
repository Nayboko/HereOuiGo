<?php
	error_reporting(E_ALL);
	empty($_SESSION)? session_start() : print "";
	include("./BD/info_bd.php");

	if(isset($_POST['tri'])){
		$tri = (int)$_POST['tri'];
	}
	
	function afficher_trajet($data){
		global $username,$password, $tri;
		echo "<table id='info_trajet'>
				<tr>
					<th>Info membre</th>
					<th>Info trajet</th>
					<th>Autre</th>
				</tr>";

			
		foreach($data as $trajet){
			$id_trajet = intval($trajet['id_trajet']);
			$bdd = new PDO('mysql:host=localhost;dbname=bdd_hereouigo;charset=utf8', $username, $password);

			// Les offres sont triées par prix par trajet
			switch($tri){
				case 1:
					$req_verif =$bdd->prepare("SELECT * FROM trajet, offre, membre WHERE trajet.id_trajet = offre.id_trajet AND trajet.id_trajet = :idtr AND offre.id_membre = membre.mail ORDER BY trajet.date_trajet ASC;");
					echo "<p>1</p>";
					break;
				case 2:
					$req_verif =$bdd->prepare("SELECT * FROM trajet, offre, membre WHERE trajet.id_trajet = offre.id_trajet AND trajet.id_trajet = :idtr AND offre.id_membre = membre.mail ORDER BY offre.prix ASC;");
					echo "<p>2</p>";
					break;
				case 3:
					$req_verif =$bdd->prepare("SELECT * FROM trajet, offre, membre WHERE trajet.id_trajet = offre.id_trajet AND trajet.id_trajet = :idtr AND offre.id_membre = membre.mail ORDER BY offre.prix DESC;");
					echo "<p>3</p>";
					break;
				default:
					$req_verif =$bdd->prepare("SELECT * FROM trajet, offre, membre WHERE trajet.id_trajet = offre.id_trajet AND trajet.id_trajet = :idtr AND offre.id_membre = membre.mail;");
			}

			//print "<p>".$tri."<p>";
			
			

			if($req_verif->execute(array("idtr" => $id_trajet))){
				$id_offre = $req_verif->fetchAll();
				//print_r($id_offre);
				foreach($id_offre as $offre){
					if($offre['nb_places'] > 0){
						echo "<tr>";
							if(isset($_SESSION['auth'])){
								echo "<td class='info_mbr'><p><a href='profil.php?pseudo={$offre['pseudo']}'>{$offre['pseudo']}</a></p></td>";

								echo "<td class='info_trajet'>
													<p><strong class='bold_vd'>{$trajet['ville_depart']}</strong> → <strong class='bold_va'>{$trajet['ville_arrivee']}</strong> </p>
													<p>Date : {$trajet['date_trajet']}</p>
													<p>Rendez-vous : {$offre['adr_rdv']}</p>
													<p>Dépôt : {$offre['adr_depot']}</p></td>";
								echo "<td class='info_autre'><p><strong class='bold_prix'>{$offre['prix']}</strong> €</p>
															<p>Places restantes : {$offre['nb_places']}</p>";
								if($offre['pseudo'] != $_SESSION['pseudo']){
									echo "<p><form action=".$_SERVER['PHP_SELF']." method=post id=".$offre['id_offre'].">
										<button type='submit' name='reservation' value=".$offre['id_offre']." form=".$offre['id_offre']."> Réserver </button>
										</form></p></td>";
								}

							}else{
									echo "<td class='info_mbr'><p>{$offre['pseudo']}</p></td>";
									echo "<td class='info_trajet'><p>Départ : {$trajet['ville_depart']}</p>
														<p>Arrivée : {$trajet['ville_arrivee']}</p>
														<p>Date : {$trajet['date_trajet']}</p></td>";
									echo "<td class='info_autre'><p>Prix : {$offre['prix']} €</p></td>";}
						echo "</tr>";
					}
				}
			}
			else{
				include("./include/formulaire_rechercher_trajet.php");
				echo "
					<div class='error_box'>
					<p>Une erreur s'est produite lors de l'execution de votre demande, veuillez réessayer !</p>
					</div>";
			}
		}
		echo "</table>";
	}
?>

<!doctype html>
<html lang="fr">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<title> HereOuiGo - voyagez tranquille </title>
		<link rel="icon" href="./WebContents/icon.png" sizes="64x64" />
		<link rel="stylesheet" href="styles.css"/>
		<script src="./Scripts/monscript.js"></script>

		<!--[if lt IE 9]>
			<script src="./Scripts/html5shiv.js"></script>
		<![endif] -->
	</head>
	<body>
		<?php
			include("./include/header.php");
		?>
		<div id="main">
			<h2>Rechercher un trajet</h2>
		<?php
			// Effectuer une réservation :
			if(isset($_POST['reservation'])){
				$resa=$_POST['reservation'];
				$mail=$_SESSION['mail'];
				try{
					// Connexion à la BDD
					$bdd = new PDO('mysql:host=localhost;dbname=bdd_hereouigo;charset=utf8', $username, $password);
					$req_verif =$bdd->prepare("SELECT * FROM passager WHERE id_offre=:offre AND mail=:mail");
					if($req_verif->execute(array("offre"=>$resa, "mail"=>$mail))){
						$data = $req_verif->fetch();
						if(count($data) > 1){
							echo "
								<div class='error_box'>
								<p>Vous avez déjà une réservation pour cette offre!</p>
								</div>";
							
						}
						else{
							$req_verif =$bdd->prepare("UPDATE offre SET nb_places=nb_places-1 WHERE id_offre=:offre");
							if($req_verif->execute(array("offre"=>$resa))){
								$req_verif =$bdd->prepare("INSERT INTO passager(id_offre,mail) VALUES (:offre, :mail);");
								if($req_verif->execute(array("offre"=>$resa, "mail"=>$mail))){
									echo "
										<div class='valid_box'>
										<p>Votre réservation a bien été prise en compte !</p>
										</div>";
									
								}
								else{
									$req_verif =$bdd->prepare("UPDATE offre SET nb_places=nb_places+1 WHERE id_offre=:offre");
									if(!$req_verif->execute(array("offre"=>$resa))){
										echo "
											<div class='error_box'>
											<p>Une erreur s'est produite lors de l'execution de votre demande, veuillez réessayer !</p>
											</div>";
										
									}

									include("./include/formulaire_rechercher_trajet.php");
									echo "
										<div class='error_box'>
										<p>Une erreur s'est produite lors de l'execution de votre demande, veuillez réessayer !</p>
										</div>";
								}
							}
							else{
								include("./include/formulaire_rechercher_trajet.php");
								echo "
									<div class='error_box'>
									<p>Une erreur s'est produite lors de l'execution de votre demande, veuillez réessayer !</p>
									</div>";
							}
						}
					}
					// On oublie pas de  fermer le curseur d'analyse des résultats après l'utilisation
					$req_verif->closeCursor();
 
					// Déconnexion de la BDD
					unset( $bdd );
				}
				catch(PDOException $e){
					print"Erreur ! : ".$e->getMessage()."</br>";
					die();					
				}
			}

			//Recherche trajets:
			if(isset($_POST['submit'])){
				$depart=strtoupper($_POST['depart']);
				$arrivee=strtoupper($_POST['arrivee']);
				$date_t=$_POST['date'];
				try{
					// Connexion à la BDD
					$bdd = new PDO('mysql:host=localhost;dbname=bdd_hereouigo;charset=utf8', $username, $password);
					// Recherche des trajets
					$req_verif =$bdd->prepare("SELECT * 
								FROM trajet 
								WHERE ville_depart = :depart
								AND ville_arrivee = :arrivee
								AND date_trajet >= :date_t;
								AND nb_places > 0");
					if($req_verif->execute(array("depart" => $depart,"arrivee"=>$arrivee,"date_t"=>$date_t))){
						$data = $req_verif->fetchAll();
						if(count($data) > 0){
							include("./include/formulaire_rechercher_trajet.php");
							afficher_trajet($data);
						}
						else{
							include("./include/formulaire_rechercher_trajet.php");
							echo "
							<div class='valid_box'>
							<p>Aucun trajet disponible !</p>
							</div>";
						}
					}
					else{
						include("./include/formulaire_rechercher_trajet.php");
						echo "
							<div class='error_box'>
							<p>Une erreur s'est produite lors de l'execution de votre demande, veuillez réessayer !</p>
							</div>";
					}
					// On oublie pas de  fermer le curseur d'analyse des résultats après l'utilisation
					$req_verif->closeCursor();
 
					// Déconnexion de la BDD
					unset( $bdd );
				}
				catch(PDOException $e){
					print"Erreur ! : ".$e->getMessage()."</br>";
					die();
				}
			}
			else{
				include("./include/formulaire_rechercher_trajet.php");
			}

		?>


		</div>
		<?php
			include("./include/footer.php");
		?>
	</body>
</html>