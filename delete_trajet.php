
<?php
    error_reporting(E_ALL);
    empty($_SESSION)? session_start() : print "";
    include("./BD/info_bd.php");
?>
<!doctype html>
<html lang="fr">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<title> HereOuiGo - voyagez tranquille </title>
		<link rel="icon" href="./WebContents/icon.png" sizes="64x64" />
		<link rel="stylesheet" href="styles.css"/>
		<script src="./Scripts/contact.js"></script>

		<!--[if lt IE 9]>
			<script src="./Scripts/html5shiv.js"></script>
		<![endif] -->
	</head>
	<body>

			<?php
				include("./include/header.php");
			?>

			<div id="main">
			<?php

        if(isset($_POST['id_offre']) && isset($_SESSION['auth'])){
                $offre=intval($_POST['id_offre']);
                try{
                    // Connexion à la BDD
                    $bdd = new PDO('mysql:host=localhost;dbname=bdd_hereouigo;charset=utf8', $username, $password);
                    $req_verif = $bdd->prepare("DELETE FROM offre
                                                WHERE id_offre = :offre;");
                    if($req_verif->execute(array('offre' => $offre))){
                        $req_verif = $bdd->prepare("SELECT * FROM passager WHERE id_offre=:offre;");
                        if($req_verif->execute(array('offre' => $offre))){
                            $data=$req_verif->fetchAll();
                            if(count($data)>=1){
                            	/*ne fonctionne pas en local. il faudrait un serveur smtp*/
                            	$nom=$_SESSION['pseudo'];
                            	$email=$_SESSION['mail'];
                            	$message1 = $nom ."(".$email.")". " a annulé le trajet, veuillez consulter vos trajets sur le site.";
                                foreach($data as $ligne){//delete de la table passager chaque passager
                                	$req_verif = $bdd->prepare("DELETE FROM passager WHERE passager.mail=:mail AND passager.id_offre = :offre;");
                                	if($req_verif->execute(array("mail" => $ligne['mail'], "offre" => $offre))){
	                                    $a = $ligne['mail'];
	                                    $sujet = "HereOuiGo : Annulation d'un trajet";
	                                    $de = "HereOuiGo";
	                                    $message2 = "Nom: ".$nom."\r\n E-mail: ".$email."\r\n Message: ".$message1;
	                                    $entete = "De: ".$de."\r\n";
	                                    $entete .= "Content-type: text/plain; charset=UTF-8" . "\r\n";
	                                    if(@mail($a,$sujet,$message2,$entete)){
	                                    	echo "<div class='valid_box'><p>L'annulation de votre trajet a bien été signalé par e-mail à ".$a." !</p>
	                                    		<p><a href='mes_trajets.php'>Retourner à mes trajets </a></p></div>";
										}
										else{
											echo "<div class='error_box'><p>Une erreur s'est produite lors de l'envoie de votre message à ".$a." !</p>
												<p><a href='mes_trajets.php'> Retourner à mes trajets !</a></p>
												</div>";
										}
									}else{echo "<div class='valid_box'><p>Votre trajet a bien été annulé mais une erreur s'est produite lors de l'envoi du mail à vos passagers !</p>
												<p><a href='mes_trajets.php'>Retourner à mes trajets </a></p></div>";
									}
                                }
                         
                            }
                            else{
                                header("Location: mes_trajets.php");
                            }
                        }
                        else{
                            echo "
                            <div class='error_box'>
                            <p>Une erreur s'est produite lors de l'envoi de votre annulation aux passagers, <a href='contact.php'>veuillez informer un administrateur!</a></p>
                            </div>";   
                        }
                    }
                    else{
                        echo "
                        <div class='error_box'>
                        <p>Une erreur s'est produite lors de l'execution de votre demande, <a href='mes_trajets.php'>veuillez réessayer !</a></p>
                        </div>";
                    }
                    // On oublie pas de  fermer le curseur d'analyse des résultats après l'utilisation 
                    $req_verif->closeCursor();
                    // Déconnexion de la BDD
                    unset( $bdd );
                }
                catch(PDOException $e){
                    print"Erreur ! : ".$e->getMessage()."</br>";
                    die();
                }
        }
        else{
          echo "
              <div class='error_box'>
               <p>Vous n'avez pas accès à cette demande.</p>
              <a href='index.php'> Retourner à l'accueil </a>
              </div>";
        }

?>
</div>
			<?php
				include("./include/footer.php");
			?>

	</body>
</html>