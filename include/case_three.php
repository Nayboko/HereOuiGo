<section id="case_three">
		<article class="case_three-img">
			<img src="./WebContents/seek.png" alt="Recherche de trajets">
			
			<h3>Chercher votre trajet</h3>
			<p>Et retrouvez toutes les offres de la communauté</p>
		</article>

		<article class="case_three-img">
			<img src="./WebContents/rating.png" alt="Consultation de profil">
			
			<h3>Vérifier qui seront vos compagnons de route</h3>
			<p>Consultez avis et profils pour choisir vos covoitureurs.</p>
		</article>

		<article class="case_three-img">
			<img src="./WebContents/ghost.png" alt="Esprit léger">

			<h3>Partez l'esprit léger !</h3>
			<p>Préparez vos valiser et soyez au point de rendez-vous !</p>
		</article>
	</section>