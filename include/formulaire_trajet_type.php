<?php
	echo "<h2>Créer un Trajet-Type</h2>";

	echo "<form class='generic_form' id='id_trajet_type' name='trajet_type' method='post' action="; echo $_SERVER['PHP_SELF'] . ">"; 
				 
	echo "
		<table>
			<tr><td><label for='ville_dep'> Ville de départ : </label></td>
			<td><input type='text' name='ville_dep' pattern='^[a-zA-Z]*(-|\s)?([a-zA-Z]*)?' autofocus required/></td></tr>
			</br>
			<tr><td><label for='ville_arr'> Ville d'arrivée : </label></td>
			<td><input type='text' name='ville_arr' maxlength='40' pattern='^[a-zA-Z]*(-|\s)?([a-zA-Z]*)?'  required/><td></tr>
			</br>
			<tr><td><label for='distance'> Distance : </label></td>
			<td><input type='text' name='distance' maxlength='4' pattern='^[0-9]*' required/></td></tr>
			</br>
			<tr><td><label for='time'> Temps moyen (hh:mm) : </label></td>
			<td><input type='text' name='time' maxlength='5' pattern='^[0-9]{2}:[0-9]{2}' required/></td></tr>
			</br>
		</table>
			<input type='submit' name='submit' value='Ajouter'/>
		
	</form>
		";


?>