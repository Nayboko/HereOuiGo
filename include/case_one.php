 
<section id="case_one">
	<?php if(!empty($_SESSION) && isset($_SESSION['auth']) && isset($_SESSION['admin'])){

				if(isset($_GET['admin'])){
					echo '<img src="./WebContents/network.jpg" alt="Network">';
				}else{
					echo '<img src="./WebContents/cuba.jpg" alt="Cuba">';

				}
			}else{
					echo '<img src="./WebContents/vw-camper.jpg" alt="Roadtrip">';

				}
		?>
	
	<article>
		
		<?php 
			if(!empty($_SESSION) && isset($_SESSION['auth']) && isset($_SESSION['admin'])){

				if(isset($_GET['admin'])){
					if(strcmp($_GET['admin'], true)){
						echo "<h2>Gestion des membres</h2>";
						echo '<p><a href="blacklist_membre.php" id="but_info_trajet">Bloquer un membre</a></p>';
						echo '<p><a href="debloquer_membre.php" id="but_info_trajet">Débloquer un membre</a></p>';
						echo '<p><a href="delete_membre.php" id="but_info_trajet">Supprimer un membre</a></p>';
						echo '<p><a href="add_admin.php" id="but_info_trajet">Ajouter un Admin</a></p>';
						echo '<p><a href="delete_admin.php" id="but_info_trajet">Supprimer un Admin</a></p>';
					}
				}else{

					echo "<h2>Bienvenue {$_SESSION['prenom']}</h2>";
					echo "<p>Pour la gestion de la communauté, veuillez cliquer sur le bouton 'Administration': </p>";
					echo '<p><a href="./mes_trajets.php" id="but_info_trajet">Mes trajets</a></p>';
					echo '<p><a href="add_vehicule.php" id="but_info_trajet">Ajouter un vehicule</a></p>';
					echo '<p><a href="add_trajet.php" id="but_info_trajet">Proposer un trajet</a></p>';
					echo '<p><a href="./rechercher_trajet.php" id="but_info_trajet">Consulter les offres</a></p>';
					echo '<p><a href="trajet_type.php" id="but_info_trajet">Créer un trajet type</a></p>';
					echo '<p><a href="stats_site.php" id="but_info_trajet">Accéder aux stats</a></p>';
				}
			}elseif(!empty($_SESSION) && isset($_SESSION['auth'])){
				echo "<h2>Bienvenue {$_SESSION['prenom']}</h2>";
				echo "<p>Voici les options qui s'offrent à vous : </p>";
				echo '<p><a href="./mes_trajets.php" id="but_info_trajet">Mes trajets</a></p>';
				echo '<p><a href="add_vehicule.php" id="but_info_trajet">Ajouter un vehicule</a></p>';
				echo '<p><a href="./rechercher_trajet.php" id="but_info_trajet">Consulter les offres</a></p>';
				echo '<p><a href="add_trajet.php" id="but_info_trajet">Proposer un trajet</a></p>';
			}else{
				echo '<h2>Besoin de changer d\'air?</h2>
					<p>Explorez les trajets proposés par les membres de la communauté et partez en voyage!</p>
					<p><a href="./rechercher_trajet.php" id="but_info_trajet">Consulter les offres</a></p>';
			}
		?>

	</article>
</section>
