<?php
	$bdd = new PDO('mysql:host=localhost;dbname=bdd_hereouigo;charset=utf8', 'root', 'root');
	$now = date('Y-m-d');
	$me = '';
	if(isset($_SESSION['auth'])){
		$me = $_SESSION['mail'];	
	}
	
 
	function compter_membre(){
		global $bdd;

		$req_verif =$bdd->prepare("SELECT COUNT(*) 
						FROM membre;");
		$req_verif->execute();
		$data = $req_verif->fetch();
		return $data['COUNT(*)'];
	}

	function compter_trajet(){
		global $bdd, $now;

		$req_verif =$bdd->prepare("SELECT COUNT(*) 
						FROM trajet WHERE trajet.date_trajet >= :today;");
		$req_verif->execute(array("today" => $now));
		$data = $req_verif->fetch();
		return $data['COUNT(*)'];
	}

	function compter_offre(){
		global $bdd, $now;

		$req_verif =$bdd->prepare("SELECT COUNT(*) 
						FROM offre, trajet WHERE trajet.id_trajet = offre.id_trajet AND trajet.date_trajet >= :now;");
		$req_verif->execute(array("now" => $now));
		$data = $req_verif->fetch();
		return $data['COUNT(*)'];
	}


	function compter_avis(){
		global $bdd;
		
		$req_verif =$bdd->prepare("SELECT COUNT(*) 
						FROM avis;");
		$req_verif->execute();
		$data = $req_verif->fetch();
		return $data['COUNT(*)'];
	}

	function compter_mes_trajets(){
		global $bdd, $me, $now;
		$cpt = 0;
		$req_verif = $bdd->prepare("SELECT COUNT(*) FROM membre, offre, trajet WHERE membre.mail = :email AND membre.mail = offre.id_membre AND offre.id_trajet = trajet.id_trajet AND trajet.date_trajet >= :now;");
		$req_verif->execute(array('email' => $me, 'now' => $now));
		$data = $req_verif->fetch();

		$cpt += $data['COUNT(*)'];

		$req_verif = $bdd->prepare("SELECT COUNT(*) FROM passager, offre WHERE passager.mail = :email AND passager.id_offre = offre.id_offre AND offre.id_trajet = trajet.id_trajet AND trajet.date_trajet >= :now;");
		$req_verif->execute(array('email' => $me, 'now' => $now));
		$data = $req_verif->fetch();

		$cpt += $data['COUNT(*)'];

		return $cpt;
	}
	function compter_avis_deposer(){	
		global $bdd, $now, $me;
		$cpt = 0;

		$req_verif = $bdd->prepare("SELECT DISTINCT COUNT(*) FROM passager, offre, trajet, trajet_type, membre WHERE passager.id_offre = offre.id_offre AND offre.id_trajet = trajet.id_trajet AND trajet_type.ville_depart = trajet.ville_depart AND trajet_type.ville_arrivee = trajet.ville_arrivee AND passager.mail = :email AND date_trajet < :now AND offre.id_membre = membre.mail AND offre.id_offre NOT IN (SELECT avis.id_offre FROM avis WHERE avis.id_membre = :user);");
		$req_verif->execute(array("email" => $me, "now" => $now, "user" => $me));
		$data = $req_verif->fetch();

		$cpt += (int)$data['COUNT(*)'];

		$req_verif = $bdd->prepare("SELECT DISTINCT COUNT(*) 
											FROM offre, membre_vehicule, vehicule, trajet, trajet_type
											WHERE offre.id_membre = :email
											AND offre.id_membre = membre_vehicule.mail
											AND membre_vehicule.immatriculation = vehicule.immatriculation
											AND offre.id_trajet = trajet.id_trajet
											AND offre.id_voiture = vehicule.immatriculation
											AND trajet.ville_depart = trajet_type.ville_depart
											AND trajet.ville_arrivee = trajet_type.ville_arrivee
											AND trajet.date_trajet < :now AND offre.id_offre NOT IN (SELECT avis.id_offre FROM avis WHERE avis.id_membre = :user);");
		$req_verif->execute(array("email" => $me, "now" => $now, "user" => $me));
		$data = $req_verif->fetch();

		$cpt += (int)$data['COUNT(*)'];

		return $cpt;

	}
	function compter_offres_membre(){
		global $bdd, $me;
		$req_verif = $bdd->prepare("SELECT COUNT(*) FROM membre, offre WHERE membre.mail = :email AND membre.mail = offre.id_membre;");
		$req_verif->execute(array('email' => $me));
		$data = $req_verif->fetch();

		return $data['COUNT(*)'];
	}

	function prix_moyen_trajet($vd, $va){
		global $bdd;
		$req = $bdd->prepare("SELECT AVG(offre.prix) FROM offre, trajet WHERE trajet.ville_depart = :vd AND trajet.ville_arrivee = :va AND trajet.id_trajet = offre.id_trajet;");
		$req->execute(array("vd" => $vd, "va" => $va));
		$data = $req->fetch();
		if($data['AVG(offre.prix)'] == NULL){
			return "Non Référencé";
		}
		return $data['AVG(offre.prix)'];
	}

	// Note moyenne conducteurs


	// Note moyenne passagers

	

?>