<?php

	function date_age_min(){
        $aujourdhui=date("Y-m-d");
        $date_naissance_min=mktime(0,0,0,substr($aujourdhui,5,2),substr($aujourdhui,8,2),substr($aujourdhui,0,4)-16);

        return date("Y-m-d",$date_naissance_min); 
    }

    echo '<h2>Inscription</h2>
    	<p>Tous les champs sont obligatoires</p>';

	echo '<form id="id_form" name="inscription" method="post" action='; echo $_SERVER['PHP_SELF'] . ">"; 
				
				echo '
				<label for="pseudo"> Pseudo : </label>
				<input type="text" name="pseudo" maxlength="64" required/>
				<br>
				<label for="password"> Mot de passe : </label>
				<input type="password" name="password" autocomplete="off" value="" autofocus required/>
				</br>
				<label for="password2"> Confirmation du mot de passe </label>
				<input type="password" name="password2" required/>
				</br>
				<label for="email_ins"> Adresse e-mail : </label>
				<input type="email" name="email" required/>
				</br>
				<label for="nom"> Nom :</label>
				<input type="text" name="nom" required/>
				</br>
				<label for="prenom"> Prenom :</label>
				<input type="text" name="prenom" required/>
				</br>
				<label for="date"> Date de naissance : </label>
				<input type="date" name="date" min="1900-01-01" max='.date_age_min().' required/>
				</br>
				<label for="numero"> Numero de telephone : </label>
				<input type="text" name="numero" maxlength="10" pattern="^[0]([0-9]{9})" required/>
				</br>

				<p> Adresse :</p>
				<label for="numvoie"> Numero de la voie : </label>
				<input type="text" name="numvoie" pattern="^[0-9]*" required/>
				</br>
				<label for="nomrue"> Nom de la rue : </label>
				<input type="text" name="nomrue" required/>
				</br>
				<label for="compl"> Complément d\'adresse : </label>
				<input type="text" name="compl"/>
				</br>
				<label for="codep"> Code postal : </label>
				<input type="text" name="codep" maxlength="5" pattern="^[0-9]{5}" required/>
				</br>
				<label for="ville"> Ville : </label>
				<input type="text" name="ville" required/>
				</br>

				<input type="submit" name="submit" value="M\'inscrire"/>
				<input type="reset" name="reset" value="Effacer" />

				</form>';
?>