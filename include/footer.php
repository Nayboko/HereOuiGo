<footer>
	<div id="foot">
		<article> 
			<h4> Info Pratique </h4>
			<ul>
				<li><a href="profil.php"> Consulter mon profil</a></li>
				<li><a href="mes_trajets.php"> Mes trajets en cours</a></li>
				<li><a href="leave_avis.php"> Déposer un avis</a></li>
				<li><a href="add_trajet.php"> Proposer un trajet</a></li>
				<li><a href="./info.php?content=faq"> Foire aux questions</a></li>
			</ul>
		</article>
		<article>
			<h4> À Propos </h4>
			<ul>
				<li><a href="./info.php?content=nous"> Notre entreprise </a></li>
				<li><a href="./info.php?content=presse"> La presse </a></li>
				<li><a href="./info.php?content=recrue"> Recrutement </a></li>
				<li><a href="./contact.php"> Nous contacter </a></li>
			</ul>
		</article>
		<article>
			<h4> Mentions légales </h4>
			<ul>
				<li><a href="./info.php?content=condutil"> Conditions d'utilisation </a></li>
				<li><a href="./info.php?content=polconf"> Politique de confidentialité </a></li>
				<li><a href="./info.php?content=cookies"> Cookies </a></li>
			</ul>
		</article>
	</div>
	<div id="copyright">
		<p>© HereOuiGo 2017 tous droits réservés - Plateforme communautaire de covoiturage</p>
	</div>
</footer>

<script src="./Scripts/jquery.js"></script> 
<script src="./Scripts/cookiechoices.js"></script>
<script>document.addEventListener('DOMContentLoaded', function(event){cookieChoices.showCookieConsentBar('Les cookies nous permettent de personnaliser le contenu et les annonces, d\'offrir des fonctionnalités relatives aux médias sociaux et d\'analyser notre trafic. Nous partageons également des informations sur l\'utilisation de notre site avec nos partenaires de médias sociaux, de publicité et d\'analyse, qui peuvent combiner celles-ci avec d\'autres informations que vous leur avez fournies ou qu\'ils ont collectées lors de votre utilisation de leurs services.', 'J’accepte', 'En savoir plus', 'info.php?content=condutil');});</script>
  